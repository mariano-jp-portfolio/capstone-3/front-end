import { Fragment, useEffect, useState, useContext } from 'react';
import { useRouter } from 'next/router';

// React-Bootstrap
import { Row, Col, Form, Button } from 'react-bootstrap';

// CSS
import styles from '../styles/Home.module.css'

// Component import
import UserContext from '../UserContext';
import Head from '../components/Head';

// helper
import AppHelper from '../helpers/app-helper';

// Google Login
import { GoogleLogin } from 'react-google-login';
const clientId = AppHelper.CLIENT_ID;

// SweetAlert2
import Swal from 'sweetalert2';

// Home page is the login page
export default function Home() {
	const headData = {
		title: 'Budget Tracker Application',
		description: 'Take charge of your financials!'
	}
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<Row className="justify-content-center">
				<Col xs md="6">
					<LoginForm />
				</Col>
			</Row>
		</Fragment>
	);
}

// Login form
const LoginForm = () => {
	// UserContext
	const { setUser } = useContext(UserContext);
	
	// router
	const router = useRouter();
	
	// State
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	
	// Authentication process
	function authenticate(e) {
		e.preventDefault();
		
		// Email Login
		// Assigning a token for the user when they login
		fetch(`${AppHelper.API_URL}/users/login`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {	
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				
				setEmail('');
				setPassword('');
			} else {
				if (data.error === 'does-not-exist') {
					Swal.fire('Authentication Failed', 'User does not exist.', 'error');
				} else if (data.error === 'incorrect-password') {
					Swal.fire('Authentication Failed', 'Please check your password.', 'error');
				} else if (data.error === 'login-type-error') {
					Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login method.', 'error');
				}
			}
		})
	}
	
	// Google Login
	const authenticateGoogleToken = (res) => {
		fetch(`${AppHelper.API_URL}/users/verify-google-token-id`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ tokenId: res.tokenId })
		})
		.then(res => res.json())
		.then(data => {
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			} else {
				if (data.error === 'google-auth-error') {
					Swal.fire('Google Auth Error', 'Google authentication procedure failed.', 'error');
				} else if (data.error === 'login-type-error') {
					Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login method.', 'error');
				}
			}
		})
	};
	
	// Fetching user's information upon login
	const retrieveUserDetails = (accessToken) => {
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ accessToken }` }
		})
		.then(res => res.json())
		.then(data => {
			// setUser
			setUser({
				id: data._id
			});
			
			// Welcome alert
			Swal.fire('Welcome!', `How's your day, ${ data.givenName }?`, 'success');
			
			// router
			router.push('/categories');
		})
	};
	
	// Effect
	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		}
	}, [email, password]);
	
	return (
		<Fragment>
			<h2>Login</h2>
			<p>Don't have an account yet? <a href="/register">Register now</a></p>
			<Form onSubmit={ authenticate }>
				<Form.Group controlId="userEmail">
					<Form.Control
						type="email"
						placeholder="Email"
						value={ email }
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="userPassword">
					<Form.Control
						type="password"
						placeholder="Password"
						value={ password }
						onChange={(e) => setPassword(e.target.value)}
						required
					/>
				</Form.Group>
				
				{ isActive ?
					<Button className="mb-3 btn btn-block" variant="primary" type="submit">
						Login
					</Button>
					:
					<Button disabled className="mb-3 btn btn-block" variant="secondary">
						Login
					</Button>
				}
				<hr />
				<GoogleLogin 
					clientId={ clientId }
					buttonText="Continue with Google"
					onSuccess={ authenticateGoogleToken }
					onFailure={ authenticateGoogleToken }
					cookiePolicy={ 'single_host_origin' }
					className="w-100 d-flex justify-content-center"
				/>
			</Form>
		</Fragment>
	);
};