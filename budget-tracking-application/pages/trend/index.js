import { Fragment, useState, useEffect } from 'react';
import moment from 'moment';

// React-Bootstrap
import { Row, Col, Form } from 'react-bootstrap'

// Component import
import Head from '../../components/Head';
import LineChart from '../../components/LineChart';

// helper
import AppHelper from '../../helpers/app-helper';

// Trend page
export default function BudgetTrend() {
	// State
	const [records, setRecords] = useState([]);
	const [dates, setDates] = useState([]);
	const [dailyAmount, setDailyAmount] = useState([]);
	const [startDate, setStartDate] = useState(new Date());
	const [endDate, setEndDate] = useState(new Date());
	
	// Effect
	// Fetch user's data
	useEffect(() => {
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			if (typeof user !== 'undefined') {
				setRecords(user.records);
			}
		});
	}, []);
	
	// loops through the records array and filters them depending on the dates
	useEffect(() => {
		let recordDates = [];
		
		records.forEach(element => {
			// date range
			if (moment(startDate).format('L') <= moment(element.createdOn).format('L') && moment(endDate).format('L') >= moment(element.createdOn).format('L')) {

				// to avoid multiple entries of the same date
				if (!recordDates.find(date => date === moment(element.createdOn).format('L'))) {
					recordDates.push(moment(element.createdOn).format('L'));
				}
			}
		});
		
		setDates(recordDates);
	}, [startDate, endDate]);

	// to set the daily amount
	useEffect(() => {
		setDailyAmount(dates.map(date => {
			// daily amount is initialized to 0
			let budget = 0;
			
			// to filter dates if they fall on Expense or Income
			records.forEach(element => {
				if (moment(element.createdOn).format('L') === date) {
					// subtract from the budget if it's an expense and add if it's an income
					if (element.type === "Expense") {
						budget -= element.amount;
					} else if (element.type === "Income") {
						budget += element.amount;
					}
				}
			});
			
			return {
				date: date,
				amount: budget
			}
		}));
	}, [dates]);

	const headData = {
		title: 'Budget Trends',
		description: 'Be in the know if you\'re budget is going up or down.'
	};
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<h2>Budget Trend</h2>
			<Row>
				<Col>
					<Form>
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control 
								type="date"
								value={ startDate }
								onChange={(e) => setStartDate(e.target.value)}
							/>
						</Form.Group>
					</Form>
				</Col>
				
				<Col>
					<Form>
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control 
								type="date"
								value={ endDate }
								onChange={(e) => setEndDate(e.target.value)}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>
			<hr />
			<LineChart budgetData={ dailyAmount } />

			<p className="text-center text-muted">X-axis: Date range & Y-axis: Budget in &#8369;</p>
		</Fragment>	
	);
}