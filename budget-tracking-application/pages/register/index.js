import { Fragment, useState, useEffect } from 'react';
import { useRouter } from 'next/router';

// React-Bootstrap
import { Row, Col, Form, Button } from 'react-bootstrap';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// SweetAlert2 
import Swal from 'sweetalert2';

// Register Page
export default function index() {
	const headData = {
		title: 'Registration Page',
		description: 'Make an account with us today.'
	}
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<Row className="justify-content-center">
				<Col xs md="6">
					<RegisterForm />
				</Col>
			</Row>
		</Fragment>
	);
}

// Registration form
const RegisterForm = () => {
	// router
	const router = useRouter();
	
	// State
	const [givenName, setGivenName] = useState('');
	const [familyName, setFamilyName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [showPassword, setShowPassword] = useState('password');
	
	// Registration process
	function registerUser(e) {
		e.preventDefault();
		
		// To look for duplicate emails in our DB
		fetch(`${AppHelper.API_URL}/users/email-exists`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ email: email })
		})
		.then(res => res.json())
		.then(data => {
			// if no dupe email, proceed with the registration
			if (data === false) {
				fetch(`${AppHelper.API_URL}/users`, {
					method: 'POST',
					headers: { 'Content-Type': 'application/json' },
					body: JSON.stringify({
						givenName: givenName,
						familyName: familyName,
						mobileNo: mobileNo,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					if (data) {
						setGivenName('');
						setFamilyName('');
						setMobileNo('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						
						Swal.fire('Success!', 'Thank you for registering with us.', 'success');
						
						// redirect
						router.push('/');
					}
				})
			} else {
				// if there's dupe email, alert the user
				Swal.fire({
					icon: 'info',
					title: 'Oops...',
					text: 'User email is already taken. Try a different one.',
					footer: `<a href="/">Go to Login page</a>`
				});
			}
		});
	}
	
	// Effect
	useEffect(() => {
		if ((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)) {
			setIsActive(true);
		}
	}, [mobileNo, email, password1, password2]);
	
	function showOrNot() {
		if (showPassword === 'password') {
			setShowPassword('text');
		} else{
			setShowPassword('password');
		}
	}
	
	return (
		<Fragment>
			<h2>Register</h2>
			<Form onSubmit={ registerUser }>
				<h6>We'll never share your details with anyone else.</h6>
				<Form.Group controlId="userGivenName">
					<Form.Control
						type="text"
						placeholder="Given name"
						value={ givenName }
						onChange={(e) => setGivenName(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="userFamilyName">
					<Form.Control
						type="text"
						placeholder="Family name"
						value={ familyName }
						onChange={(e) => setFamilyName(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="userMobileNo">
					<Form.Control
						type="text"
						placeholder="11-digit mobile number (09xx)"
						value={ mobileNo }
						onChange={(e) => setMobileNo(e.target.value)}
						maxLength="11"
						required
					/>
				</Form.Group>
				
				<br />
				<h6>Login credentials</h6>
				
				<Form.Group controlId="userEmail">
					<Form.Control
						type="email"
						placeholder="Email"
						value={ email }
						onChange={(e) => setEmail(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="userPassword1">
					<Form.Control
						type={ showPassword }
						placeholder="Password"
						value={ password1 }
						onChange={(e) => setPassword1(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="userPassword2">
					<Form.Control
						type={ showPassword }
						placeholder="Verify password"
						value={ password2 }
						onChange={(e) => setPassword2(e.target.value)}
						required
					/>
				</Form.Group>
				
				<Form.Group controlId="showPassword">
					<Form.Check type="checkbox" label="Show password" onClick={ showOrNot } />
				</Form.Group>
				
				{ isActive ?
					<Button className="mb-3 btn btn-block" variant="primary" type="submit">
						Submit
					</Button>
					:
					<Button disabled className="mb-3 btn btn-block" variant="secondary">
						Submit
					</Button>
				}
			</Form>
		</Fragment>
	);
};