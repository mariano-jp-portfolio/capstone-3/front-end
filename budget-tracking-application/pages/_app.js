import React, { Fragment, useState, useEffect } from 'react';

// React-Bootstrap
import { Container } from 'react-bootstrap';

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// Global CSS
import '../styles/globals.css';

// Provider
import { UserProvider } from '../UserContext';

// Component import
import Navbar from '../components/Navbar';

// helper
import AppHelper from '../helpers/app-helper';

// Root component
export default function MyApp({ Component, pageProps }) {
	// User State
	const [user, setUser] = useState({ id: null });
	
	// This will ensure that the localStorage can be accessed at least once after this component has been rendered
	// localStorage seems to be refreshed after every rendering of our application, that's why a fetch request of the user's details
	useEffect(() => {	
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }` }
		})
		.then(res => res.json())
		.then(data => {
			if (typeof data._id !== 'undefined') {
				setUser({ id: data._id });
			} else {
				setUser({ id: null });
			}
		})
	}, [user.id]);
	
	// Testing for the effect hook's functionality
	useEffect(() => {
		console.log(user.id);
	}, [user.id]);
	
	// Clearing local storage upon user's logout
	const unsetUser = () => {
		localStorage.clear();
		
		// set the user's credential back to null
		setUser({ id: null });
	};
	
	return (
		<Fragment>
			<UserProvider value={{ user, setUser, unsetUser}}>
				<Navbar />
				<Container className="mt-3">
					<Component {...pageProps} />
				</Container>
			</UserProvider>
		</Fragment>
	);
}