import { Fragment, useState, useEffect } from 'react';

// React-Bootstrap
import { Row, Col, Table, Button } from 'react-bootstrap';

// Component import
import Head from '../../components/Head';

// helper
import AppHelper from '../../helpers/app-helper';

// Categories page
export default function Categories() {
	const headData = {
		title: 'Categories',
		description: 'View your income or expenses here.'
	};
	
	return (
		<Fragment>
			<Head dataProp={headData} />
			<Row>
				<Col>
					<h2>Categories</h2>
					<Button href="/categories/create" variant="primary">
						Add New Category
					</Button>
					<CategoriesTable />
				</Col>
			</Row>
		</Fragment>
	);
}

// Category table
const CategoriesTable = () => {	
	// State - categoriesData will store all the income and expense categories and it shall be initialized as a blank array
	const [categoriesData, setCategoriesData] = useState([]);
	
	// Effect
	useEffect(() => {
		// Everytime our root file re-renders, an effect hook will be triggered. This hook will get the token for us. All that we have to do is to call it everytime we fetch. AppHelper is really a big help this time. 
		fetch(`${AppHelper.API_URL}/users/details`, {
			headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
		})
		.then(res => res.json())
		.then(user => {
			// console.log(user)
			if (typeof user !== 'undefined') {
				setCategoriesData(user.categories.map(category => {
					// console.log(category.name);
					return (
						<tr key={category._id}>
							<td>{category.type}</td>
							<td>{category.name}</td>
							<td>
								<Button variant="warning">
									Update
								</Button>
								<Button variant="danger" className="ml-2">
									Delete
								</Button>
							</td>
						</tr>
					);
				}));
			}
		});
	}, []);
	
	return (
		<Table className="mt-3" striped bordered>
			<thead>
				<tr>
					<th>Type</th>
					<th>Name</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				{ categoriesData }
			</tbody>
		</Table>
	);
};