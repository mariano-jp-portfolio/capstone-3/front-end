import React from 'react';

// This will create a Context Object
const UserContext = React.createContext();

// Provider component allows other components to subscribe to context changes
export const UserProvider = UserContext.Provider;

export default UserContext;